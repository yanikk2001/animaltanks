// Player contoller

#include "PlayerControllerBase.h"

void APlayerControllerBase::SetPLayerEnabledState(bool SetPlayerEnabled)
{
    if (SetPlayerEnabled)
        GetPawn()->EnableInput(this);
    else
        GetPawn()->DisableInput(this);

    bShowMouseCursor = SetPlayerEnabled;
}

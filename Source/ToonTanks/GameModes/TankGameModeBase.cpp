// Main Game Mode

#include "TankGameModeBase.h"
#include "ToonTanks/Pawns/PawnTank.h"
#include "ToonTanks/Pawns/PawnTurret.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/FileSystem/SaveAndReadResults.h"
#include "Misc/FileHelper.h"

const FString DefaultSaveDir = "C:/Unreal/AnimalTanks/SaveLogs/ScoreLog.txt";

void ATankGameModeBase::BeginPlay()
{
    Super::BeginPlay();

    HandleGameStart();
}

void ATankGameModeBase::HandleGameStart()
{
    TargetTurrets = GetTargetTurrets();
    PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0)); //0 - player index, single player game
    PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
    PlayerControllerRef->Possess(UGameplayStatics::GetPlayerPawn(this, 0));

    UGameplayStatics::PlaySound2D(this, InGameMusic);

    GameStart();

    if (PlayerControllerRef) //Prevents Calling pawn`s controller if destroyed
    {
        PlayerControllerRef->SetPLayerEnabledState(false); //Freezes player pawn at start

        EnablePlayerContollerCountdown();
    }
}

void ATankGameModeBase::HandleGameOver(bool PlayerWon)
{
    if(PlayerWon)
    {
        FString projectDir = DefaultSaveDir;
        
        WriteToFileScore(projectDir);
        TArray<FString> scores;
        FFileHelper::LoadFileToStringArray(scores, *projectDir);

        float *floatScores = ConvertStringToFloatArr(scores);
        if (!floatScores)
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to initialise float arr"));
            GameOver(PlayerWon);
            return;
        }

        float HighestScore = GetHighestScore(floatScores, scores.Num());
        float CurrentScore = floatScores[scores.Num() -1];
        delete[] floatScores;
        UE_LOG(LogTemp, Warning, TEXT("%f"), HighestScore);

        GameOverWithStats(HighestScore, CurrentScore);


        // SaveAndReadResults* fileSystem = nullptr;
        // const std::string date = std::string(TCHAR_TO_UTF8(*FDateTime::Now().GetDate().ToString()));//converts FString to std::string
        // float highScore = 0.0;
        // try
        // {
        //     fileSystem = &SaveAndReadResults::GetInstance("ScoresLog.txt");
        //     fileSystem->AddNewScore(date, GetGameTimeSinceCreation());
        //     fileSystem->ReadFile();
        //     highScore = fileSystem->GetHighScore();
        // }
        // catch(const std::invalid_argument& e)
        // {
        //     UE_LOG(LogTemp, Error, TEXT("hellow1"));
        //     UE_LOG(LogTemp, Error, TEXT("%s"), *FString(e.what()));
        //     GameOver(PlayerWon);
        //     return;
        // }
        // catch(const std::exception& e)
        // {
        //     UE_LOG(LogTemp, Error, TEXT("hellow"));
        //     UE_LOG(LogTemp, Error, TEXT("%s"), *FString(e.what()));
        //     GameOver(PlayerWon);
        //     return;
        // }
        // catch(...)
        // {
        //     UE_LOG(LogTemp, Error, TEXT("Unknown Error"));
        //     GameOver(PlayerWon);
        //     return;
        // }
    }
    else
    {
        GameOver(PlayerWon);
    }
}

int32 ATankGameModeBase::GetTargetTurrets() const
{
    TArray<AActor *> TurretActors;
    //Finds all turrets in the world and puts them in an array
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), TurretActors);

    return TurretActors.Num();
}

void ATankGameModeBase::EnablePlayerContollerCountdown()
{
    FTimerHandle PlayerEnableHandle;
    FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef,
                                        &APlayerControllerBase::SetPLayerEnabledState, true);
    GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, (float)StartDelay, false);
}

void ATankGameModeBase::WriteToFileScore(const FString &filepath) const
{
    FString currentTime = FString::SanitizeFloat(GetGameTimeSinceCreation());
    FString newLine = "\n";

    if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*filepath))
    {
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("** Could not Find File **"));
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("** Creating new one in C:/Unreal/AnimalTanks/SaveLogs/ScoreLog.txt**"));
    }

    FFileHelper::SaveStringToFile(currentTime, *filepath, FFileHelper::EEncodingOptions::AutoDetect,
                                  &IFileManager::Get(), EFileWrite::FILEWRITE_Append);

    FFileHelper::SaveStringToFile(newLine, *filepath, FFileHelper::EEncodingOptions::AutoDetect,
                                  &IFileManager::Get(), EFileWrite::FILEWRITE_Append);
}

float *ATankGameModeBase::ConvertStringToFloatArr(TArray<FString> &strArr)
{
    float *scoresFloat = nullptr;
    int32 size = strArr.Num();
    try
    {
        scoresFloat = new float(size);

        for (int32 i = 0; i < size; i++)
        {
            scoresFloat[i] = FCString::Atof(*strArr[i]);
        }
    }
    catch (const std::bad_alloc &e)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(e.what()));
        return nullptr;
    }

    return scoresFloat;
}

float ATankGameModeBase::GetHighestScore(const float* scores, const int32& size) const
{
    if(!scores)
        return 0;
    
    float highestScore = scores[0];
    for (int32 i = 0; i < size; i++)
    {
        if (scores[i] < scores[++i])
            highestScore = scores[i];
    }

    return highestScore;
}

void ATankGameModeBase::ActorDied(AActor *DeadActor)
{
    if (DeadActor == PlayerTank)
    {
        PlayerTank->HandleDestruction();
        HandleGameOver(false);

        if (PlayerControllerRef)
        {
            PlayerControllerRef->SetPLayerEnabledState(false);
        }
    }
    else if (APawnTurret *DestroyedTurret = Cast<APawnTurret>(DeadActor)) //If cast is successful then it is a turret
    {
        DestroyedTurret->HandleDestruction();
        TargetTurrets -= 1;

        if (TargetTurrets == 0)
        {
            HandleGameOver(true);
        }
    }
}

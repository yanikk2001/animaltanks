// Starting screen

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StartingScreen.generated.h"

class APlayerControllerBase;
UCLASS()
class TOONTANKS_API AStartingScreen : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Music")
	USoundBase* MenuMusic = nullptr;

	APlayerControllerBase* PlayerControllerRef = nullptr;
protected:
	virtual void BeginPlay() override;
};

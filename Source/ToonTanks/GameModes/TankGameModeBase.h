// Main Game Mode

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TankGameModeBase.generated.h"

class APawnTank;
class APawnTurret;
class APlayerControllerBase;
UCLASS()
class TOONTANKS_API ATankGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	FString StartTime;
	
	/*
		Starts the game, gets turret number and ref to player
	*/
	void HandleGameStart();

	/*
		Freezes input and stops the Tick function
	*/
	void HandleGameOver(bool PlayerWon);

	//Gets number of turrets in the level
	int32 GetTargetTurrets() const;

	//Enables input after x amount of seconds
	void EnablePlayerContollerCountdown();

	void WriteToFileScore(const FString& filename) const;
	float* ConvertStringToFloatArr(TArray<FString>& strArr);
	float GetHighestScore(const float* scores, const int32& size) const;

	APawnTank *PlayerTank = nullptr;
	APlayerControllerBase *PlayerControllerRef = nullptr;
	int32 TargetTurrets = 0;

	UPROPERTY(EditAnywhere, Category = "Music")
	USoundBase *InGameMusic = nullptr;

public:
	/*
		If actor is the player - calls Handlegameover func else if turret count of turrets -=1
		and checks for win condition
		@param ref to actor that died
	*/
	void ActorDied(AActor *DeadActor);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Loop");
	int32 StartDelay = 3;

	virtual void BeginPlay() override;
	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();
	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool PlayerWon);
	UFUNCTION(BlueprintImplementableEvent)
	void GameOverWithStats(float stats, float currentScore);
};

// Starting screen

#include "StartingScreen.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"

void AStartingScreen::BeginPlay()
{
    PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
    PlayerControllerRef->SetPLayerEnabledState(false);

    UGameplayStatics::PlaySound2D(this, MenuMusic);
}

#pragma once

#include <iostream>
#include <fstream>
#include <vector>

struct DateAndTime
{
    std::string date;
    float time;
};

class SaveAndReadResults
{
    std::fstream file;
    std::string filepath;
    std::vector<std::string> dates;
    std::vector<float> times;

    /*
        File Size
        @param filePath file path
        @return value of seekg()
    */
    static std::size_t getFileSize(const char *filePath);

    /*
        Writes to file the contents of struct DateAndTime
        @param out - ostream, dateTime
    */
    friend void operator<<(std::ostream &out, const DateAndTime &dateTime);

    /*
        Fills the class vectors with the new input
    */
    void ReadDateTime();

    /*
        Private Constructor
        @exception if filepath is invalid or can`t open file
    */
    SaveAndReadResults(const char *filename);

    /*
        Checks for valid file size
    */
    bool IsFileValid();

    /*  
        Calls getFileSize
        @return pointer to dynamically allocated size_t - size of file
        @info returns nullptr if it catches an exception
    */
    std::size_t *InitialiseFileSize();

public:
    /*
        Ensures only one instance of the class exists
        @return ref to static var
    */
    static SaveAndReadResults &GetInstance(const char *filename);

    SaveAndReadResults(const SaveAndReadResults &other) = delete;
    SaveAndReadResults &operator=(const SaveAndReadResults &other) = delete;
    SaveAndReadResults(const SaveAndReadResults &&other) = delete;
    SaveAndReadResults &operator=(const SaveAndReadResults &&other) = delete;

    /*
        Calls ReadDateTime
        @exception if file is not open or is not the right size
        @return true if operation is successfull, otherwise - false
    */
    bool ReadFile();

    /*
        Writes to the file at the end
        @exception if file is not open or is not the right size
        @param date 
        @param Time in seconds
    */
    void AddNewScore(std::string date, float timeInSec);

    /*
        Finds the highest value in vector times
    */
    const float GetHighScore();

    // Closes the file
    ~SaveAndReadResults();
};
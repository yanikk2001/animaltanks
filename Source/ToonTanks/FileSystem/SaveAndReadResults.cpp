#include "SaveAndReadResults.h"
#define MIN_FILE_SIZE (std::size_t)12

SaveAndReadResults::SaveAndReadResults(const char *filename)
{
    if (!filename)
        throw std::invalid_argument("Invalid Filename");

    file.open(filename, std::ios_base::in | std::ios_base::out | std::ios_base::app);

    if (!file.is_open())
    {
        std::logic_error e("Failed to open file");
        throw std::exception(e);
    }

    this->filepath = filename;
}

SaveAndReadResults &SaveAndReadResults::GetInstance(const char *filename)
{
    try
    {
        static SaveAndReadResults instance(filename);
        return instance;
    }
    catch (const std::invalid_argument &e)
    {
        throw e;
    }
    catch (const std::exception &e)
    {
        throw e;
    }
}

std::size_t SaveAndReadResults::getFileSize(const char *filePath)
{
    if (!filePath)
    {
        throw std::invalid_argument("filePath is nullptr");
    }

    std::ifstream file(filePath, std::ios::binary | std::ios::in);
    if (!file)
    {
        throw std::runtime_error("Could not open file in getFileSize(const char *)");
    }

    file.seekg(0, std::ios::end);
    std::size_t size = file.tellg();
    file.close();
    return size;
}

void SaveAndReadResults::ReadDateTime()
{
    std::string tmpStr;
    file >> tmpStr;
    dates.push_back(tmpStr);

    float time = 0;
    file >> time;
    times.push_back(time);
}

bool SaveAndReadResults::ReadFile()
{
    if (!file.is_open())
    {
        std::logic_error e("Failed to open file");
        throw std::exception(e);
    }

    std::size_t *fileSize = InitialiseFileSize();
    if (!fileSize)
    {
        throw std::runtime_error("Failed to initialise size of file in ReadFile");
    }

    if (*fileSize < MIN_FILE_SIZE)
    {
        delete fileSize;
        return false;
    }

    file.seekg(0, std::ios_base::beg);
    while ((std::size_t)file.tellg() <= *fileSize)
    {
        ReadDateTime();
    }

    delete fileSize;
    return true;
}

void SaveAndReadResults::AddNewScore(std::string date, float timeInSec)
{
    if (!file.is_open())
    {
        std::logic_error e("Failed to open file");
        throw std::exception(e);
    }

    DateAndTime dateTime = {date, timeInSec};

    file.seekp(std::ios_base::ate);
    file << dateTime;
}

bool SaveAndReadResults::IsFileValid()
{
    std::size_t *size = InitialiseFileSize();
    if (&size)
    {
        return (*size > MIN_FILE_SIZE);
    }

    return false;
}

std::size_t *SaveAndReadResults::InitialiseFileSize()
{
    std::size_t *size = nullptr;
    try
    {
        std::size_t tmpSize = getFileSize(this->filepath.c_str());
        size = new std::size_t;
        *size = tmpSize;
    }
    catch (const std::invalid_argument &e)
    {
        std::cerr << e.what() << '\n';
        return nullptr;
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << e.what() << '\n';
        return nullptr;
    }
    catch (const std::bad_alloc &e)
    {
        std::cerr << e.what() << '\n';
        return nullptr;
    }

    return size;
}

void operator<<(std::ostream &out, const DateAndTime &dateTime)
{
    out << dateTime.date;
    out << ' ';
    out << dateTime.time;
    out << '\n';
}

const float SaveAndReadResults::GetHighScore()
{
    if (!file.is_open())
    {
        std::logic_error e("Failed to open file");
        throw std::exception(e);
    }

    if (IsFileValid())
    {
        std::size_t size = times.size();

        float highestScore = times[0];
        for (std::size_t i = 0; i < size; i++)
        {
            if (times[i] > times[++i])
                highestScore = times[i];
        }

        return highestScore;
    }

    return 0;
}

SaveAndReadResults::~SaveAndReadResults()
{
    file.close();
}

// Base class for PawnTank and PawnTurret

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnBase.generated.h"

class UCapsuleComponent; //Collision class
class AProjectileBase;
class UHealthComponent;
class UMatineeCameraShake;

UCLASS()
class TOONTANKS_API APawnBase : public APawn
{
	GENERATED_BODY()

	//COMPONENTS
	//simple collision information for all child classes
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true")) // Unreal standart for properties with 'U' prefix
	UCapsuleComponent *CapsuleComp = nullptr;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *BaseMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TurretMesh = nullptr;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USceneComponent *ProjectileSpawnPoint = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UHealthComponent *HealthComponent = nullptr;

	//VARIABLES
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile Type", meta = (AllowPrivateAccess = true));
	TSubclassOf<AProjectileBase> ProjectileClass; //Makes sure that only the correct class can be assigned

	UPROPERTY(EditAnywhere, Category = "Effects")
	UParticleSystem *DeathParticle = nullptr;
	UPROPERTY(EditAnywhere, Category = "Effects")
	USoundBase *DeathSound;
	UPROPERTY(EditAnywhere, Category = "Effects")
	TSubclassOf<UMatineeCameraShake> DeathShake;

public:
	// Sets default values for this pawn's properties
	APawnBase();

	/*	
		Handles particle system, sound and camera shake at death
	*/
	virtual void HandleDestruction();

protected:
	/* 
		Rotates turret mesh on the X and Y axis only at target
		@param Coordinates of Target
	*/
	void RotateTurretFunction(FVector LookAtTarget);

	/*
		Spawns instance of ProjectileBase in the world
		@exception if it fails to allocate projectile class
		@info sets this class instance as owner to prevent friendly damage
	*/
	void Fire();
};

// Pawn Tank

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnTank.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class TOONTANKS_API APawnTank : public APawnBase
{
	GENERATED_BODY()
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"));
	USpringArmComponent *SpringArm = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"));
	UCameraComponent *Camera = nullptr;

	//TOP MESH
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TankLitMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *AnimalMesh = nullptr;

	//TIRES MESH
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TireFrontLeft = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TireFrontRight = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TireBackLeft = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *TireBackRight = nullptr;

	FVector MoveDirection;
	FQuat RotationDirection;

	UPROPERTY(EditAnywhere)
	float MoveSpeed = 500.0;
	UPROPERTY(EditAnywhere)
	float RotateSpeed = 75.0;

	APlayerController *PlayerControllerRef = nullptr;

	bool IsPlayerAlive = true;

	/*
		Takes a value based on player input, Only updates th e X coordinates
		@param takes -1 if pressed key "S", +1 if pressed key is "W"
		@info coordinates are multiplied by Delta time to stay framerate independent 
	*/
	void CalculateMoveInput(float Value);

	/* 
		Takes a value based on player input, Only updates th e Y coordinates
		@param takes -1 if pressed key "A", +1 if pressed key is "D"
		@info coordinates are multiplied by Delta time to stay framerate independent
	*/
	void CalculateRotateInput(float Value);

	void Move();
	void Rotate();

	/*
		Traces from the Tank to location to cursor to get rotation
		@exception if it fails to allocate PlayerController
	*/
	void RotateTankTurret();

public:
	APawnTank();

	/*
	Called every frame
	@param DeltaTime time passed after last frame
	*/
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	/*
		Overrides PawnBase func, sets playerAlive bool to false and disables input and visibility
	*/
	virtual void HandleDestruction() override;

	const bool &GetIsPlayerAlive() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};

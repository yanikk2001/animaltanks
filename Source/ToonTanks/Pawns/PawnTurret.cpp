// Pawn Turret

#include "PawnTurret.h"
#include "Kismet/GameplayStatics.h"
#include "PawnTank.h"

// Called when the game starts or when spawned
void APawnTurret::BeginPlay()
{
    Super::BeginPlay();

    //sets timer that call every x seconds a function, x = FireRate
    GetWorld()->GetTimerManager().SetTimer(FireRateTimerHandle, this, &APawnTurret::CheckFireCondition, FireRate, true);
    //Checks if there is a valid player in the world and initialises it
    PlayerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void APawnTurret::HandleDestruction()
{
    Super::HandleDestruction();
    Destroy();
}

// Called every frame
void APawnTurret::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    SetRotationAtTarget();
}

void APawnTurret::CheckFireCondition()
{
    if (!PlayerPawn || !PlayerPawn->GetIsPlayerAlive())
        return;
    if (ReturnDistanceToPlayer() <= FireRange)
    {
        Fire();
    }
}

float APawnTurret::ReturnDistanceToPlayer()
{
    if (!PlayerPawn)
        return 0.0;

    return FVector::Dist(PlayerPawn->GetActorLocation(), this->GetActorLocation());
}

void APawnTurret::SetRotationAtTarget()
{
    if (!PlayerPawn || ReturnDistanceToPlayer() > FireRange)
        return;

    this->RotateTurretFunction(PlayerPawn->GetActorLocation());
}

// PawnTank

#include "PawnTank.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

APawnTank::APawnTank()
{
    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
    SpringArm->SetupAttachment(RootComponent);

    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->SetupAttachment(SpringArm);

    //TOP MESHES
    TankLitMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tank Lit Mesh"));
    TankLitMesh->SetupAttachment(TurretMesh);
    AnimalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Animal Mesh"));
    AnimalMesh->SetupAttachment(TurretMesh);

    //TIRE MESHES
    TireFrontLeft = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tire Front Left"));
    TireFrontLeft->SetupAttachment(BaseMesh);
    TireFrontRight = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tire Front Right"));
    TireFrontRight->SetupAttachment(BaseMesh);
    TireBackLeft = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tire Back Left"));
    TireBackLeft->SetupAttachment(BaseMesh);
    TireBackRight = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tire Back Right"));
    TireBackRight->SetupAttachment(BaseMesh);
}

// Called when the game starts or when spawned
void APawnTank::BeginPlay()
{
    Super::BeginPlay();

    PlayerControllerRef = Cast<APlayerController>(GetController());
}

void APawnTank::HandleDestruction()
{
    Super::HandleDestruction();
    IsPlayerAlive = false;

    SetActorHiddenInGame(true);
    SetActorTickEnabled(false); //stops the tick function
}

const bool &APawnTank::GetIsPlayerAlive() const
{
    return this->IsPlayerAlive;
}

// Called every frame
void APawnTank::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    Rotate(); // Rotate is first, so it calculates movement direction first before actual movement
    Move();

    RotateTankTurret();
}

// Called to bind functionality to input
void APawnTank::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAxis("MoveForward", this, &APawnTank::CalculateMoveInput);
    PlayerInputComponent->BindAxis("Turn", this, &APawnTank::CalculateRotateInput);
    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APawnTank::Fire);
}

void APawnTank::CalculateMoveInput(float Value)
{
    MoveDirection = FVector(Value * MoveSpeed * GetWorld()->DeltaTimeSeconds, 0, 0);
}

void APawnTank::CalculateRotateInput(float Value)
{
    float RotateAmout = Value * RotateSpeed * GetWorld()->DeltaTimeSeconds;
    FRotator Rotation(0, RotateAmout, 0);
    RotationDirection = FQuat(Rotation); // converts FRotator to quaternion
}

void APawnTank::Move()
{
    AddActorLocalOffset(MoveDirection, true); // bool bsweep = true means collisions are on
}

void APawnTank::Rotate()
{
    AddActorLocalRotation(RotationDirection, true);
}

void APawnTank::RotateTankTurret()
{
    if (!PlayerControllerRef)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to allocate player contoller"));
        return;
    }
    FHitResult TraceHitResult;
    PlayerControllerRef->GetHitResultUnderCursor(ECC_Visibility, false, TraceHitResult);
    FVector HitLocation = TraceHitResult.ImpactPoint; //gets only the end of the hit result

    RotateTurretFunction(HitLocation);
}

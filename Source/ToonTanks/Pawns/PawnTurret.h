// Pawn Turret

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnTurret.generated.h"

class APawnTank;
UCLASS()
class TOONTANKS_API APawnTurret : public APawnBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Combat", meta = (AllowPrivateAccess = "true"));
	float FireRate = 2.0; // Interval of time in seconds during which turrets are passive
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Combat", meta = (AllowPrivateAccess = "true"));
	float FireRange = 500.0; // Field of view of turret

	FTimerHandle FireRateTimerHandle;
	APawnTank* PlayerPawn = nullptr;

	/*
		Called every x amount of seconds, calls fire() only if conditions are met
	*/
	void CheckFireCondition();

	// Returns 0 if there is no player
	float ReturnDistanceToPlayer();

	void SetRotationAtTarget();
	
public:
	/*
	Called every frame
	@param DeltaTime time passed after last frame
	*/
	virtual void Tick(float DeltaTime) override;

	virtual void HandleDestruction() override;

protected: 
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};

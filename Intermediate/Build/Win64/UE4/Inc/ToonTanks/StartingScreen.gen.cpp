// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ToonTanks/GameModes/StartingScreen.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStartingScreen() {}
// Cross Module References
	TOONTANKS_API UClass* Z_Construct_UClass_AStartingScreen_NoRegister();
	TOONTANKS_API UClass* Z_Construct_UClass_AStartingScreen();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ToonTanks();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase_NoRegister();
// End Cross Module References
	void AStartingScreen::StaticRegisterNativesAStartingScreen()
	{
	}
	UClass* Z_Construct_UClass_AStartingScreen_NoRegister()
	{
		return AStartingScreen::StaticClass();
	}
	struct Z_Construct_UClass_AStartingScreen_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MenuMusic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MenuMusic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStartingScreen_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ToonTanks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStartingScreen_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GameModes/StartingScreen.h" },
		{ "ModuleRelativePath", "GameModes/StartingScreen.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStartingScreen_Statics::NewProp_MenuMusic_MetaData[] = {
		{ "Category", "Music" },
		{ "ModuleRelativePath", "GameModes/StartingScreen.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AStartingScreen_Statics::NewProp_MenuMusic = { "MenuMusic", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AStartingScreen, MenuMusic), Z_Construct_UClass_USoundBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AStartingScreen_Statics::NewProp_MenuMusic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AStartingScreen_Statics::NewProp_MenuMusic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AStartingScreen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AStartingScreen_Statics::NewProp_MenuMusic,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStartingScreen_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStartingScreen>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStartingScreen_Statics::ClassParams = {
		&AStartingScreen::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AStartingScreen_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AStartingScreen_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AStartingScreen_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStartingScreen_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStartingScreen()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStartingScreen_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStartingScreen, 617778643);
	template<> TOONTANKS_API UClass* StaticClass<AStartingScreen>()
	{
		return AStartingScreen::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStartingScreen(Z_Construct_UClass_AStartingScreen, &AStartingScreen::StaticClass, TEXT("/Script/ToonTanks"), TEXT("AStartingScreen"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStartingScreen);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOONTANKS_StartingScreen_generated_h
#error "StartingScreen.generated.h already included, missing '#pragma once' in StartingScreen.h"
#endif
#define TOONTANKS_StartingScreen_generated_h

#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_SPARSE_DATA
#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_RPC_WRAPPERS
#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStartingScreen(); \
	friend struct Z_Construct_UClass_AStartingScreen_Statics; \
public: \
	DECLARE_CLASS(AStartingScreen, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ToonTanks"), NO_API) \
	DECLARE_SERIALIZER(AStartingScreen)


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAStartingScreen(); \
	friend struct Z_Construct_UClass_AStartingScreen_Statics; \
public: \
	DECLARE_CLASS(AStartingScreen, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ToonTanks"), NO_API) \
	DECLARE_SERIALIZER(AStartingScreen)


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStartingScreen(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStartingScreen) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStartingScreen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStartingScreen); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStartingScreen(AStartingScreen&&); \
	NO_API AStartingScreen(const AStartingScreen&); \
public:


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStartingScreen(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStartingScreen(AStartingScreen&&); \
	NO_API AStartingScreen(const AStartingScreen&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStartingScreen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStartingScreen); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStartingScreen)


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MenuMusic() { return STRUCT_OFFSET(AStartingScreen, MenuMusic); }


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_10_PROLOG
#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_PRIVATE_PROPERTY_OFFSET \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_SPARSE_DATA \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_RPC_WRAPPERS \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_INCLASS \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_PRIVATE_PROPERTY_OFFSET \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_SPARSE_DATA \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_INCLASS_NO_PURE_DECLS \
	AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOONTANKS_API UClass* StaticClass<class AStartingScreen>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AnimalTanks_Source_ToonTanks_GameModes_StartingScreen_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
